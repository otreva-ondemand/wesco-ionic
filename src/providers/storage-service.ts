import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class StorageService {
  constructor(private nativeStorage: NativeStorage) { }

  get(key: string) {
    return new Promise((resolve, reject) => {
      this.nativeStorage.getItem(key).then(
        data => resolve(data),
        error => reject(error)
      );
    });
  }

  set(key: string, value: any) {
    return new Promise((resolve, reject) => {
      this.nativeStorage.setItem(key, value).then(
        () => resolve(),
        error => reject(error)
      );
    });
  }

  remove(key: string) {
    return new Promise((resolve, reject) => {
      this.nativeStorage.remove(key).then(
        () => resolve(),
        error => reject(error)
      );
    });
  }
}
