import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { HttpService } from './http-service';
import { StorageService } from './storage-service';

@Injectable()
export class AuthService {
  // Observable to allow user auth state to dynamically
  // control the value of rootPage on the app component
  rootPageSource = new Subject<string>();
  rootPage$ = this.rootPageSource.asObservable();

  constructor(
    public httpService: HttpService,
    public storageService: StorageService
  ) { }

  setRootPage(rootPage: string) {
    this.rootPageSource.next(rootPage);
  }

  setAuthToken(authToken) {
    return new Promise((resolve, reject) => {
      let tokenObject = {
        authToken: authToken
      };

      this.storageService.set('auth-token', tokenObject).then(() => {
        resolve();
      }).catch(error => {
        reject(error);
      });
    });
  }

  setUserCredentials(username, password) {
    let passwordBase64 = btoa(password);

    let credsObject = {
      username: username,
      passwordBase64: passwordBase64
    };

    this.storageService.set('user-creds', credsObject).catch(error => {
      console.log(error);
    });
  }

  getUserCredentials() {
    return new Promise((resolve, reject) => {
      this.storageService.get('user-creds').then((data: any) => {
        let userCreds = {
          username: data.username,
          password: atob(data.passwordBase64)
        };
        resolve(userCreds);
      }).catch(error => {
        reject(error);
      });
    });
  }

  getAuthToken() {
    return new Promise((resolve, reject) => {
      this.storageService.get('auth-token').then((data: any) => {
        resolve(data.authToken);
      }).catch(error => {
        resolve(null);
      });
    });
  }

  removeAuthToken() {
    this.storageService.remove('auth-token');
  }

  setUserAuthState() {
    this.getAuthToken().then(authToken => {
      if (authToken === null) {
        this.getUserCredentials().then(userCreds => {
          this.reauthenticateUser();
        }).catch(error => {
          this.setRootPage('LoginPage');
        });
      } else {
        this.setRootPage('HomePage');
      }
    });
  }

  reauthenticateUser() {
    this.getUserCredentials().then((userCreds: any) => {
      let httpBody = {
        email: userCreds.username,
        vipcode: userCreds.password
      };

      this.httpService.post('login', httpBody, {}).then((res: any) => {
        let authToken = JSON.parse(res.data);
        this.setAuthToken(authToken.AuthToken);

        this.setRootPage('HomePage');
      }).catch(error => {
        this.removeAuthToken();

        if (error.status === 403) {
          this.setRootPage('NotAuthorizedPage');
        } else {
          this.setRootPage('LoginPage');
        }
    });
    });
  }

  authenticateUser(username: string, password: string) {
    return new Promise((resolve, reject) => {
      username = username.toLowerCase();

      if (username === '' || password === '') {
        reject('You must supply a username and a password.');
        return;
      };

      let httpBody = {
        email: username,
        vipcode: password
      };

      this.httpService.post('login', httpBody, {}).then((res: any) => {
        this.setUserCredentials(username, password);
        let authToken = JSON.parse(res.data);
        this.setAuthToken(authToken.AuthToken);
        this.setRootPage('HomePage');
        resolve();
      }).catch(error => {
        let message: any = {};

        try {
          message = JSON.parse(error.error);
        } catch(error) {
          message.Message = 'An error has occured';
        }

        if (error.status === 403) {
          this.setUserCredentials(username, password);
          this.removeAuthToken();
          this.setRootPage('NotAuthorizedPage');
          reject();
          return;
        }

        reject(message.Message);
      });
    });
  }
}
