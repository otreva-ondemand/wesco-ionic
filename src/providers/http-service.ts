import { Injectable } from '@angular/core';
import { HTTP } from 'ionic-native';

import { Globals } from '../app/globals';

@Injectable()
export class HttpService {
  constructor(public globals: Globals) { }

  post(url: string, params: any, headers: any) {
    return new Promise((resolve, reject) => {
      HTTP.post(this.globals.endpointUrl + url, params, headers).then(res => {
        resolve(res);
      }).catch(error => {
        reject(error);
      });
    });
  }

  get(url: string, params: any, headers: any) {
    return new Promise((resolve, reject) => {
      HTTP.get(this.globals.endpointUrl + url, params, headers).then(res => {
        resolve(res);
      }).catch(error => {
        reject(error);
      });
    });
  }
}
