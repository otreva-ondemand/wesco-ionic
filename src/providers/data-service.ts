import { Injectable } from '@angular/core';

import { AuthService } from './auth-service';
import { HttpService } from './http-service';

@Injectable()
export class DataService {
  constructor(
    public authService: AuthService,
    public httpService: HttpService
  ) { }

  getHomeData() {
    return new Promise((resolve, reject) => {
      this.authService.getAuthToken().then(authToken => {
        this.httpService.get('home/' + authToken, {}, {}).then((res: any) => {
          let data = JSON.parse(res.data);
          resolve({
            firstName: data.FirstName,
            lastName: data.LastName,
            salesActual: data.SalesActual,
            salesGoal: data.SalesGoal,
            salesPercent: data.SalesPercent,
            feedback: data.Feedback,
            announcementTitle: data.AnnouncementHeader,
            announcementCreatedDate: data.AnnouncementCreatedOn
          });
        }).catch(error => {
          this.authService.reauthenticateUser();
          reject(error);
        });
      }).catch(error => {
        reject(error);
      });
    });
  }

  getAnnouncementData() {
    return new Promise((resolve, reject) => {
      this.authService.getAuthToken().then(authToken => {
        this.httpService.get('announcements/' + authToken, {}, {}).then((res: any) => {
          let data = JSON.parse(res.data);
          data = this.cleanAnnouncementData(data);
          resolve(data);
        }).catch(error => {
          this.authService.reauthenticateUser();
          reject(error);
        });
      }).catch(error => {
        reject(error);
      });
    });
  }

  cleanAnnouncementData(announcements) {
    let cleanAnnouncements: any = [];
    for (let i = 0; i < announcements.length; i++) {
      cleanAnnouncements.push({
        createdOn: announcements[i].CreatedOn,
        header: announcements[i].Header,
        html: announcements[i].Html
      });
    }

    return cleanAnnouncements;
  }
}
