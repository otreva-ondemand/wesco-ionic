import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import { AuthService } from '../../providers/auth-service';
import { Globals } from '../../app/globals';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  userEmail: string = '';
  userPassword: string = '';
  showError: boolean = false;
  errorMessage: string = '';
  loadingSpinner: any;

  constructor(
    public authService: AuthService,
    public loadingCtrl: LoadingController,
    public globals: Globals
  ) {}

  authenticateUser() {
    this.loadingSpinner = this.loadingCtrl.create({ content: this.globals.loadingText });
    this.loadingSpinner.present();
    this.showError = false;
    this.errorMessage = '';

    this.authService.authenticateUser(this.userEmail, this.userPassword).then(() => {
      this.loadingSpinner.dismiss();
    }).catch(error => {
      this.loadingSpinner.dismiss();
      this.errorMessage = error;
      this.showError = true;
    });
  }
}
