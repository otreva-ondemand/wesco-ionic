import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-push-notification-modal',
  templateUrl: 'push-notification-modal.html'
})
export class PushNotificationModalPage {
  announcementHtml: string;
  announcementTitle: string;

  constructor(
    public viewCtrl: ViewController,
    navParams: NavParams
  ) {
    this.announcementHtml = navParams.get('html');
    this.announcementTitle = navParams.get('title');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
