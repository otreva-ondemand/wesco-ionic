import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

import { AnnouncementSinglePage } from '../announcement-single/announcement-single';
import { DataService } from '../../providers/data-service';
import { Globals } from '../../app/globals';

@Component({
  selector: 'page-announcements',
  templateUrl: 'announcements.html'
})
export class AnnouncementsPage {
  announcements: any = [];
  loadingSpinner: any;
  openFirstAnnouncement: boolean;

  constructor(
    public navCtrl: NavController,
    public dataService: DataService,
    public globals: Globals,
    public loadingCtrl: LoadingController,
    public params: NavParams
  ) {
    this.listenForAppResume();
  }

  ngOnInit() {
    this.openFirstAnnouncement = this.params.get('openFirstAnnouncement');
    this.getData();
  }

  listenForAppResume() {
    document.addEventListener('resume', () => {
      if (this.navCtrl.getActive().component.name === 'AnnouncementsPage') {
        this.openFirstAnnouncement = false;
        this.getData();
      }
    });
  }

  getData() {
    this.loadingSpinner = this.loadingCtrl.create({ content: this.globals.loadingText });
    this.loadingSpinner.present();
    this.dataService.getAnnouncementData().then((res: any) => {
      this.announcements = res;
      this.loadingSpinner.dismiss();
      if (this.openFirstAnnouncement) {
        this.navCtrl.push(AnnouncementSinglePage, { announcement: this.announcements[0] })
      }
    }).catch(error => {
      console.log(error);
      this.loadingSpinner.dismiss();
    });
  }

  goToAnnouncementsingle(announcement) {
    this.navCtrl.push(AnnouncementSinglePage, { announcement: announcement });
  }
}
