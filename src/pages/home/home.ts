import { Component, NgZone } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import { NavController } from 'ionic-angular';
import { AnnouncementsPage } from '../announcements/announcements';
import { DataService } from '../../providers/data-service';
import { Globals } from '../../app/globals';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userData: any = {};
  loadingSpinner: any;
  salesActualTextLeftOffset: number = 0;
  showAnnouncements: boolean = false;
  currentYear: number = new Date().getFullYear();
  displayContent: boolean = false;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public dataService: DataService,
    public globals: Globals,
    public loadingCtrl: LoadingController
  ) {
    this.listenForAppResume();
  }

  ngOnInit() {
    this.getData();
  }

  listenForAppResume() {
    document.addEventListener('resume', () => {
      if (this.navCtrl.getActive().component.name === 'HomePage') {
        this.getData();
      }
    })
  }

  getData() {
    this.loadingSpinner = this.loadingCtrl.create({ content: this.globals.loadingText });
    this.loadingSpinner.present();
    this.dataService.getHomeData().then((res: any) => {
      res.salesActualString = '$' + res.salesActual.toLocaleString();
      res.salesGoalString = '$' + res.salesGoal.toLocaleString();
      this.userData = res;
      if (this.userData.announcementTitle !== null) {
        this.showAnnouncements = true;
      }
      this.displayContent = true;
      this.positionSalesActualTextUnderSlider();
      this.loadingSpinner.dismiss();
    }).catch(error => {
      console.log(error);
      this.loadingSpinner.dismiss();
    });
  }

  onResize() {
    this.positionSalesActualTextUnderSlider();
  }

  positionSalesActualTextUnderSlider() {
    let salesPercent = (this.userData.salesActual / this.userData.salesGoal) * 100;
    let elementWidth = window.innerWidth * 0.92;
    let leftPixels = (salesPercent / 100) * elementWidth - 50;
    let distanceFromRightEdge = (leftPixels + 100) - elementWidth;
    if (distanceFromRightEdge > 0) {
      leftPixels -= distanceFromRightEdge;
    }
    this.salesActualTextLeftOffset = leftPixels;

    // This function will force a component redraw, which
    // may be necessary for mobile after an orientation change.
    this.zone.run(() => {});
  }

  goToAnnouncements() {
    this.navCtrl.push(AnnouncementsPage, { openFirstAnnouncement: false });
  }

  goToFirstAnnouncement() {
    this.navCtrl.push(AnnouncementsPage, { openFirstAnnouncement: true });
  }
}
