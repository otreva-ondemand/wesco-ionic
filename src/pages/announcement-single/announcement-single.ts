import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-announcement-single',
  templateUrl: 'announcement-single.html'
})
export class AnnouncementSinglePage {
  announcement: any;

  constructor(
    public params: NavParams,
    public navCtrl: NavController
  ) {
    this.announcement = params.get('announcement');
  }

  goToHome() {
    this.navCtrl.setRoot(HomePage);
  }
}
