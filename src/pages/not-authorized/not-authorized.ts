import { Component } from '@angular/core';

import { AuthService } from '../../providers/auth-service';

@Component({
  selector: 'page-not-authorized',
  templateUrl: 'not-authorized.html'
})
export class NotAuthorizedPage {
  constructor(public authService: AuthService) {
    this.listenForAppResume();
  }

  listenForAppResume() {
    document.addEventListener('resume', () => {
      this.authService.reauthenticateUser();
    })
  }
}
