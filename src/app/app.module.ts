import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { NativeStorage } from '@ionic-native/native-storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AnnouncementsPage } from '../pages/announcements/announcements';
import { LoginPage } from '../pages/login/login';
import { NotAuthorizedPage } from '../pages/not-authorized/not-authorized';
import { AuthService } from '../providers/auth-service';
import { HttpService } from '../providers/http-service';
import { DataService } from '../providers/data-service';
import { Globals } from './globals';
import { AnnouncementSinglePage } from '../pages/announcement-single/announcement-single';
import { StorageService } from '../providers/storage-service';
import { PushNotificationModalPage } from '../pages/push-notification-modal/push-notification-modal';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'eaa27712'
  },
  'push': {
    'sender_id': '622548365881',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#ffffff'
      }
    }
  }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AnnouncementsPage,
    LoginPage,
    NotAuthorizedPage,
    AnnouncementSinglePage,
    PushNotificationModalPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AnnouncementsPage,
    LoginPage,
    NotAuthorizedPage,
    AnnouncementSinglePage,
    PushNotificationModalPage
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: IonicErrorHandler
    },
    AuthService,
    HttpService,
    DataService,
    StorageService,
    NativeStorage,
    Globals
  ]
})
export class AppModule {}
