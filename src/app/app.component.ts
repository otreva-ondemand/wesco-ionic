import { Component } from '@angular/core';
import { Platform, LoadingController, ModalController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Subscription } from 'rxjs/Subscription';
import { Push, PushToken } from '@ionic/cloud-angular';

import { AuthService } from '../providers/auth-service';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { NotAuthorizedPage } from '../pages/not-authorized/not-authorized';
import { Globals } from './globals';
import { PushNotificationModalPage } from '../pages/push-notification-modal/push-notification-modal';

@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>'
})
export class MyApp {
  // The root page is changed via observable on the auth service
  rootPageSubscription: Subscription;
  rootPage: any
  loadingSpinner: any;

  constructor(
    public platform: Platform,
    public authService: AuthService,
    public loadingCtrl: LoadingController,
    public globals: Globals,
    public push: Push,
    public modalCtrl: ModalController
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      setTimeout(() => { Splashscreen.hide() }, 200);

      // Subscribe to push notifications
      if (platform.is('cordova')) {
        this.registerPushNotifications();
      } else {
        console.log('Not registering push notifications; Cordova is not available.');
      }

      // Subscribe to and trigger the auth service to send the user
      // auth state so that we can display a Page
      this.loadingSpinner = this.loadingCtrl.create({ content: this.globals.loadingText });
      this.loadingSpinner.present();
      this.subscribeToRootPage();
      this.authService.setUserAuthState();
    });
  }

  registerPushNotifications() {
    this.push.register().then((t: PushToken) => {
      return this.push.saveToken(t);
    });

    this.push.rx.notification().subscribe((msg) => {
      setTimeout(() => {
        let modal = this.modalCtrl.create(PushNotificationModalPage, { 'html': msg.text, 'title': msg.title });
        modal.present();
      }, 1000);
    });
  }

  subscribeToRootPage() {
    this.rootPageSubscription = this.authService.rootPage$.subscribe(rootPage => {
      this.loadingSpinner.dismiss();

      if (rootPage === 'HomePage') {
        this.rootPage = HomePage;
      } else if (rootPage === 'NotAuthorizedPage') {
        this.rootPage = NotAuthorizedPage;
      } else if (rootPage === 'LoginPage') {
        this.rootPage = LoginPage
      }
    });
  }
}
