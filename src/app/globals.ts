import { Injectable } from '@angular/core';

Injectable()
export class Globals {
  loadingText: string = 'Loading...';
  endpointUrl: string = 'https://myaccount.wescovip.com/api/';
}
